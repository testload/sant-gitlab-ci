#!/usr/bin/env groovy
@Grapes([
        @Grab(group='cloud.testload', module='sant-library', version='1.9.2')
])
import cloud.testload.SANTLib;

serviceId=args[0]; //additional ID for service
profileName=args[1] //profile name
//Envs
System.setProperty("docker.cert.path", "/home/groovy/certs")
workDir=System.getenv('WORKING_COPY')

def date = new Date()
def runId=date.format('yy-MM-dd_HH-mm')
def santClass=new SANTLib(script:this,
        influxHost:System.getenv('INFLUX_HOST'),
        influxPort:System.getenv('INFLUX_PORT'),
        influxLogin:System.getenv('INFLUX_LOGIN'),
        influxPassword:System.getenv('INFLUX_PASSWORD'),
        jmeterBaseImage: System.getenv('JMETER_BASE_IMAGE'),
        dockerAddress: System.getenv('DOCKER_ADDRESS'),
        loaderId:serviceId,
        gitToken:System.getenv('TOKEN'));
        
println "Execute for profile:"+profileName

//load profile from JSON-file
File f = new File(workDir+File.separator+"Profiles"+File.separator+profileName+'.json')
def jsonText = f.getText()

if (!serviceId.equals("COLLECT")) // if only COLLECT stats
{

    println "Clean docker loadservices before start!"
    santClass.dockerCleanServices(jsonText);

    println "Create docker loadservices set"
    santClass.dockerCreateServices(profileName,runId,jsonText)
    santClass.waitBuffer();

    println "Waiting"
    santClass.waitLoading(jsonText);

    println "Clean docker loadservices after loading!"
    santClass.dockerCleanServices(jsonText);
}
if (!serviceId.equals("DEBUG")) // if only debug run without publish
{
    println "Collect stats from InfluxDB and publish af JSON-file"
    File dirR = new File(workDir+File.separator+"Results");
    if (!dirR.exists()) dirR.mkdirs();
    File dirP = new File(workDir+File.separator+"public");
    if (!dirP.exists()) dirP.mkdirs();
    
    File g = new File(workDir+File.separator+"Results"+File.separator+profileName+'*'+runId+'.json')
                g.createNewFile()
                g.setText(groovy.json.JsonOutput.prettyPrint(santClass.influxDBCollectResults(profileName,runId,jsonText)))
    println "Rebuild public HTMLs"
    santClass.jsonToHtmlBuilder(workDir+File.separator,"Results","Profiles","public")
}

def echo(text)
{
    println text;
}