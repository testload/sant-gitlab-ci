# Let's create image with groovy and few important libraries
FROM registry.gitlab.com/testload/sant-gitlab-ci/groovy-base

COPY *.groovy /home/groovy/
RUN set -o errexit -o nounset \
	&& echo "Caching Groovy libraries" \
	&& groovy cache_grapes.groovy